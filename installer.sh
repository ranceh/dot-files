#!/bin/bash

#functions

install_pkg () {
        pkg="$1"
        installed=$(pacman -Q $pkg 2>/dev/null | grep $pkg | wc -l)
        if [[ $installed == 0 ]]; then
            sudo pacman --needed --noconfirm -S "$pkg"
        fi
}



install_yay () {
    if ! [[ -f /usr/bin/yay ]]; then
        if [[ -f /etc/pacman.d/endeavouros-mirrorlist ]]; then
            install_pkg yay
        else
            OD = $PWD
            mkdir ~/yay && cd ~/yay
            git clone https://aur.archlinux.org/yay-git.git
            cd yay-git
            makepkg -si
            cd $OD
        fi
    fi
}

install_aur () {
    pkg="$1"
    installed=$(pacman -Q $pkg | grep $pkg | wc -l)
        if [[ $installed == 0 ]]; then
            if [[ -x /usr/bin/yay ]]; then
                AUR_CMD="yay -S"
            fi

        if [[ -x /usr/bin/paru ]]; then
            AUR_CMD="paru -S"
        fi

        if [[ ! $AUR_CMD ]]; then
            echo "AUR helper not installed, installing yay"
            install_yay
            AUR_CMD = "yay -S"
        fi
    
        $AUR_CMD $pkg
    fi
}

copy_config () {

    echo "copying configuration into user location"

    cp -r ./.config/alacritty ~/.config/
    cp -r ./.config/qtile ~/.config/
    cp -r ./.config/rofi ~/.config/
    cp ./.config/systemd/user/aur-update* ~/.config/systemd/user
    cp -r ./bin ~/
    
}

install_qtile () {

    echo "Installing qtile"
    install_pkg qtile
    echo "Installing brightnessctl"
    install_pkg brightnessctl
    echo "Installing playerctl"
    install_pkg playerctl
    echo "Installing dunst"
    install_pkg dunst
    echo "Installing udiskie"
    install_pkg udiskie
    echo "Installing rofi"
    install_pkg rofi
    echo "Installing alacritty"
    install_pkg alacritty
    echo "Installing python-dbus-next"
    install_pkg python-dbus-next
    echo "Installing nitrogen"
    install_pkg nitrogen
    echo "Installing nerd_fonts"
    install_aur nerd-fonts-complete

    copy_config
}

install_lightdm () {

    #chose lightdm greeter
    PS3="Choose a greeter for lightdm: "

    select greeter in gtk pantheon slick webkit ukui
    do
        case $greeter in 
            'gtk')
                install_pkg lightdm-gtk-greeter
                install_pkg lightdm-gtk-greeter-settings
            ;;
            'pantheon')
                install_pkg lightdm-patheon-greeter
            ;;
            'slick')
                install_pkg lightdm-slick-greeter
            ;;
            'webkit')
                install_pkg lightdm-webkit2-greeter
            ;;
            'ukui')
                install_pkg lightdm-ukui-greeter
            ;;
        esac
        PS3="Would you like to start lightdm in graphical boot"
        select option in yes no
        do
            if [[ $option == "yes" ]]; then
            sudo systemctl set-default graphical.target
            sudo systemctl enable lightdm
            fi
            break
        done
        break
    done
}

fix_mouse_speed () {
    sudo cp ./etc/X11/xorg.conf.d/50-mouse-acceleration.conf /etc/X11/xorg.conf.d/
}

setup_sshagent () {
    cp ./.config/systemd/user/ssh-agent.service ~/.config/systemd/user/
    cp ./.pam_environment ~/
    sudo systemctl daemon-reload
    systemctl --user --now enable ssh-agent.service
}

prep_endeavouros_repo () {
    if dialog --default-button "no" --yesno "May I add the EndeavourOS repo to your Arch box?  (Helpful but not required)" 0 0; then
        sudo cp ./etc/pacman.d/endeavouros-mirrorlist /etc/pacman.d/
        sudo echo "[endeavouros]" >> /etc/pacman.conf
        sudo echo "SigLevel = PackageRequired" >> /etc/pacman.conf
        sudo echo "Include = /etc/pacman.d/endeavouros-mirrorlist" >> /etc/pacman.conf
        sudo pacman-key --keyserver keyserver.ubuntu.com -r 003DB8B0CB23504F
        sudo pacman-key --lsign 003DB8B0CB23504F
        sudo pacman -Syyuu
    fi
}

#determine distro

if [ -f /etc/os-release ]; then
    #freedesktop and systemd standard
    . /etc/os-release
    OS=$NAME
else
    echo "Unable to determine OS"
    exit 1;
fi

if ! [[ $OS == "EndeavourOS" || $OS == "Arch" ]]; then
    echo "Unsupported Distro"
    exit 2;
fi

if [[ $OS == "Arch" ]]; then
    prep_endeavouros_repo    
fi

#begin install

PS3="This script will install and configure an instance of qtile (with supporting software like rofi and dunst). Would you like to proceed? "

select option in yes no
do
    if [[ $option == "yes" ]]; then
        install_qtile
        break
    else
        exit 0
    fi
done

PS3="Some systems (mostly Macs) artifically adjust mouse speed. Would you like to fix this? "

select option in yes no
do
    if [[ $option == "yes" ]]; then
        fix_mouse_speed
    fi
    break
done

PS3="Would you like to configure systemd to run a user based ssh-agent service? (optional, but suggested) "

select option in yes no
do
    if [[ $option == "yes" ]]; then
        setup_sshagent
    fi
    break
done

PS3="Would you like to install Spotify? (optional) "

select option in yes no
do
    if [[ $option == "yes" ]]; then
        install_aur spotify
    fi
    break
done

PS3="Would you like to install lightdm for graphical boot? (optional) "

select option in yes no
do
    if [[ $option == "yes" ]]; then
        install_lightdm
    fi
    break
done
