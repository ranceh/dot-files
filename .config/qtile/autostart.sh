#!/bin/sh

# Autostart script for Qtile

#cmd_exist() { unalias "$1" >/dev/null 2>&1 ; command -v "$1" >/dev/null 2>&1 ;}
#__kill() { kill -9 "$(pidof "$1")" >/dev/null 2>&1 ; }
#__start() { sleep 1 && "$@" >/dev/null 2>&1 & }
#__running() { pidof "$1" >/dev/null 2>&1 ;}


check_and_start() {
    #check if cmd exists
    if command -v "$1" >/dev/null 2>&1 ; then
        # check if running
        if ! pgrep -f "$1" >/dev/null 2>&1 ; then
            sleep 2 && "$1" > /dev/null 2>&1 &
        fi
    fi
}

# Set the wallpaper using nitrogen
# nitrogen exits after setting the wallpaper, so no need to use the management function
nitrogen --restore

# Apps to autostart

check_and_start picom

# Authentication dialog

check_and_start /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1

# Notification daemon

check_and_start dunst

# Unclutter

check_and_start unclutter

# Screensaver

check_and_start xscreensaver -no-splash

# udiskie for automouting usb drives

check_and_start udiskie

# bluetooth control

check_and_start blueberry-tray

# nextcloud

check_and_start nextcloud
