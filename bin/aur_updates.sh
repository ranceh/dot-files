#!/bin/bash

DIR="$HOME/bin/aur_updates"

if [[ ! -d $DIR ]]; then
	mkdir -p $DIR
fi

if ! updates_aur=$(yay -Qum 2> /dev/null | wc -l); then
    updates_aur=0
fi

echo "$updates_aur" > $DIR/aur_updates.data
yay -Qu > $DIR/aur_updates.list


#logging

log_length=$(wc -l $DIR/aur_updates.log | awk '{print $1}')

curr_time=$(date "+%Y.%m.%d-%H:%M:%S")

if [[ "$log_length" -gt 10 ]]; then

	echo "$curr_time - updates: $updates_aur" > $DIR/aur_updates.log

else

	echo "$curr_time - updates: $updates_aur" >> $DIR/aur_updates.log
	
fi

