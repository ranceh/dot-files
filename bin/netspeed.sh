#!/bin/bash


IFACE="$(nmcli device | grep -w connected | awk '{ print $1 }' | head -n 1)"

 
R1=`cat /sys/class/net/$IFACE/statistics/rx_bytes`
T1=`cat /sys/class/net/$IFACE/statistics/tx_bytes`
sleep 1
R2=`cat /sys/class/net/$IFACE/statistics/rx_bytes`
T2=`cat /sys/class/net/$IFACE/statistics/tx_bytes`
TBPS=`expr $T2 - $T1`
RBPS=`expr $R2 - $R1`
TKBPS=`expr $TBPS / 1024`
RKBPS=`expr $RBPS / 1024`
icon="⬇️"
icon1="⬆️"
echo "$RKBPS kB/s/$TKBPS kB/s"

