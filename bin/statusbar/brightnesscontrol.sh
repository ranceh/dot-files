#!/bin/bash

# You can call this script like this:
# brightnessControl up
# brightnessControl down

# Script inspired by these wonderful people:
# https://github.com/dastorm/volume-notification-dunst/blob/master/volume.sh
# https://gist.github.com/sebastiencs/5d7227f388d93374cebdf72e783fbd6a

DEVICE="intel_backlight"

# use brightnessctl from the command line to determine the name of your device

function send_notification {
  icon=/usr/share/icons/Papirus-Dark/16x16/actions/brightnesssettings.svg
  brightness=$(brightnessctl --device=$DEVICE get)
  maxbright=$(brightnessctl --device=$DEVICE max)
  # Make the bar with the special character ─ (it's not dash -)
  # https://en.wikipedia.org/wiki/Box-drawing_character
  #bar=$(seq -s "─" 0 $((brightness/maxbright*100 )) | sed 's/[0-9]//g')
  brightnessrate=$(echo "p=$brightness/$maxbright*100; scale=0; p/1" | bc -l)
  brightnessratescale=$(echo "scale=0; $brightnessrate/3" | bc -l)
  bar=$(seq -s "─" 0 $brightnessratescale | sed 's/[0-9]//g')
  #echo $bar
  #echo $test
  # Send the notification
  dunstify -i "$icon" -r 5555 -u normal "$bar  $brightnessrate"
}

case $1 in
  up)
    # increase the backlight by 1%
    brightnessctl --device=$DEVICE set +1%
    send_notification
    canberra-gtk-play -i audio-volume-change
    ;;
  down)
    # decrease the backlight by 1%
    brightnessctl --device=$DEVICE set 1%-
    send_notification
    canberra-gtk-play -i audio-volume-change
    ;;
  *)
    brightness=$(brightnessctl --device=$DEVICE get)
    maxbright=$(brightnessctl --device=$DEVICE max)
    brightnessrate=$(echo "p=$brightness/$maxbright*100; scale=0; p/1" | bc -l)
    icon=""
    printf "%s" "$icon $brightnessrate" "%"
    ;;
esac
