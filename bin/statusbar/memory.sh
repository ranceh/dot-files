#!/bin/bash

# memory usage script

MEMUSED=$(free | awk '/Mem/ {printf "%dM\n", $3 / 1024.0 }')

#echo $MEMUSED

ICON="﬙"

printf "%s" "$ICON $MEMUSED"