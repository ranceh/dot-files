#!/bin/sh

if ! updates_arch=$(checkupdates 2> /dev/null | wc -l ); then
    updates_arch=0
fi

updates_aur=$(cat ~/bin/aur_updates/aur_updates.data)

echo "PKG/AUR: $updates_arch/$updates_aur"
